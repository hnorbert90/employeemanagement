﻿namespace EmployeeManagement.Web.Seed
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Commands;
    using EmployeeManagement.Domain;
    using EmployeeManagement.Persistence;

    using MediatR;

    using Newtonsoft.Json;

    public class DataInitializer
    {
        private readonly IMediator _mediator;
        private readonly EmployeeManagementDbContext _dbContext;

        public DataInitializer(IMediator mediator, EmployeeManagementDbContext dbContext)
        {
            _mediator = mediator;
            _dbContext = dbContext;
        }

        public async Task SeedData()
        {
            if (!_dbContext.OrganisationalUnits.Any())
            {
                var organisationUnitsJSON = File.ReadAllText(Path.Combine("Seed", "OrganisationalUnit.json"));
                var organisationalUnits = JsonConvert.DeserializeObject<List<OrganisationalUnit>>(organisationUnitsJSON);

                _dbContext.OrganisationalUnits.AddRange(organisationalUnits);
                _dbContext.SaveChanges();
            }

            if (!_dbContext.Employees.Any())
            {
                var employeesJSON = File.ReadAllText(Path.Combine("Seed", "Employee.json"));
                var employees = JsonConvert.DeserializeObject<List<Employee>>(employeesJSON);
                foreach (var employee in employees)
                {
                    _dbContext.Add(employee);
                    await _mediator.Send(new RegisterUserCommand
                    {
                        UserName = employee.Name.Replace(' ', '.'),
                        Password = "Password123@",
                        ConfirmPassword = "Password123@",
                        EmployeeId = employee.Id
                    });
                }

                _dbContext.SaveChanges();
            }
        }
    }
}
