import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Employee } from './components/employee/Employee';
import { EmployeeEditor } from './components/employee/EmployeeEditor';
import { OrganisationalUnit } from './components/organisational-unit/OrganisationalUnit';
import { OranisationalUnitEditor } from './components/organisational-unit/OranisationalUnitEditor';
import AuthorizeRoute from './components/api-authorization/AuthorizeRoute';
import ApiAuthorizationRoutes from './components/api-authorization/ApiAuthorizationRoutes';
import { ApplicationPaths } from './components/api-authorization/ApiAuthorizationConstants';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
            <Route exact path='/' component={Home} />
            <AuthorizeRoute path='/employees' component={Employee} />
            <AuthorizeRoute path='/employee-editor/:crudType/:id?' component={EmployeeEditor} />
            <AuthorizeRoute path='/organisational_units' component={OrganisationalUnit} />
            <AuthorizeRoute path='/oranisational-unit-editor/:crudType/:id?' component={OranisationalUnitEditor} />
            <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes} />
      </Layout>
    );
  }
}
