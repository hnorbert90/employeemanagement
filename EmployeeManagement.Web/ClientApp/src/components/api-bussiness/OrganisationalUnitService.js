﻿import authService from '../api-authorization/AuthorizeService'

export class OranisationalUnitService {
    static LIST_URL = 'OrganisationalUnit/List'
    static LIST_NAMES_URL = 'OrganisationalUnit/ListOrganisationalUnitNames'
    static CREATE_URL = 'OrganisationalUnit/Create'
    static READ_URL = 'OrganisationalUnit?Id='
    static UPDATE_URL = 'OrganisationalUnit/Update'
    static DELETE_URL = 'OrganisationalUnit/Delete?id='

    async getAccessToken() {
        return await authService.getAccessToken();
    }

    async list(page, size) {
        const token = await this.getAccessToken();
        return await fetch(OranisationalUnitService.LIST_URL + "?pageIndex=" + page + "&pageSize=" + size, {
            headers: { 'Authorization': `Bearer ${token}` }
        });
    }

    async listOrganisationalUnitNames() {
        const token = await this.getAccessToken();
        return await fetch(OranisationalUnitService.LIST_NAMES_URL, {
            headers: { 'Authorization': `Bearer ${token}` }
        });
    }

    async delete(id) {
        const token = await this.getAccessToken();
        return await fetch(OranisationalUnitService.DELETE_URL + id, {
            headers: { 'Authorization': `Bearer ${token}` },
            method: 'DELETE'
        });
    }

    async read(id) {
        const token = await this.getAccessToken();
        return await fetch(OranisationalUnitService.READ_URL + id, {
            headers: {'Authorization': `Bearer ${token}`}
        });
    }

    async create(model) {
        const token = await this.getAccessToken();
        return await fetch(OranisationalUnitService.CREATE_URL, {
            headers: !token ? {} : { 'Authorization': `Bearer ${token}`, 'content-type': "application/json; charset=utf-8" },
            method: 'POST',
            body: JSON.stringify(model),
        });
    }

    async update(model) {
        const token = await this.getAccessToken();
        return await fetch(OranisationalUnitService.UPDATE_URL, {
            headers: !token ? {} : { 'Authorization': `Bearer ${token}`, 'content-type': "application/json; charset=utf-8" },
            method: 'PUT',
            body: JSON.stringify(model),
        });
    }
}

const organisationalUnitService = new OranisationalUnitService();

export default organisationalUnitService;