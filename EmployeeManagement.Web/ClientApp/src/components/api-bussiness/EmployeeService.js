﻿import authService from '../api-authorization/AuthorizeService'

export class EmployeeService {
    static LIST_URL = 'Employee/List'
    static LIST_NAMES_URL = 'Employee/ListEmployeeNames'
    static CREATE_URL = 'Employee/Create'
    static READ_URL = 'Employee?Id='
    static UPDATE_URL = 'Employee/Update'
    static DELETE_URL = 'Employee/Delete?id='

    async getAccessToken() {
        return await authService.getAccessToken();
    }

    async list(page, size) {
        const token = await this.getAccessToken();
        return await fetch(EmployeeService.LIST_URL + "?pageIndex=" + page + "&pageSize=" + size, {
            headers: { 'Authorization': `Bearer ${token}` }
        });
    }

    async listEmployeeNames() {
        const token = await this.getAccessToken();
        return await fetch(EmployeeService.LIST_NAMES_URL, {
            headers: { 'Authorization': `Bearer ${token}` }
        });
    }

    async delete(id) {
        const token = await this.getAccessToken();
        return await fetch(EmployeeService.DELETE_URL + id, {
            headers: { 'Authorization': `Bearer ${token}` },
            method: 'DELETE'
        });
    }

    async read(id) {
        const token = await this.getAccessToken();
        return await fetch(EmployeeService.READ_URL + id, {
            headers: { 'Authorization': `Bearer ${token}` }
        });
    }

    async create(model) {
        const token = await this.getAccessToken();
        return await fetch(EmployeeService.CREATE_URL, {
            headers: !token ? {} : { 'Authorization': `Bearer ${token}`, 'content-type': "application/json; charset=utf-8" },
            method: 'POST',
            body: JSON.stringify(model),
        });
    }

    async update(model) {
        const token = await this.getAccessToken();
        return await fetch(EmployeeService.UPDATE_URL, {
            headers: !token ? {} : { 'Authorization': `Bearer ${token}`, 'content-type': "application/json; charset=utf-8" },
            method: 'PUT',
            body: JSON.stringify(model),
        });
    }
}

const employeeService = new EmployeeService();

export default employeeService;