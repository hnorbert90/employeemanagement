﻿import React, { Component } from 'react';
import Swal from 'sweetalert2'
import { Link } from "react-router-dom";
import employeeService from './../api-bussiness/EmployeeService'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import RefreshIcon from '@material-ui/icons/Refresh';
import VisibilityIcon from '@material-ui/icons/Visibility';
import TablePagination from '@material-ui/core/TablePagination';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';


export class Employee extends Component {
    static displayName = Employee.name;

    constructor(props) {
        super(props);
        this.state = { employees: [], loading: true, page: 1, total: 1, size: 10 };

        this.handlePageChanged = this.handlePageChanged.bind(this);
        this.handleChangePageSize = this.handleChangePageSize.bind(this);
        this.count = this.count.bind(this);
        this.classes = makeStyles({
            table: {
                minWidth: 650,
            }
        });
    }

    componentDidMount() {
        this.loadEmployees();
    }

    async handlePageChanged(e, value) {
        await this.setState({ page: value + 1 });
        await this.loadEmployees();
    }

    async handleChangePageSize(e, value) {
        await this.setState({ size: value.props.value });
        await this.loadEmployees();
    }

    count() {
        return this.state.size * this.state.total;
    }

    renderEmployeeTable(list) {
        return (
            <TableContainer component={Paper}>
                <div className="row justify-content-between pt-2">
                    <div className="col-1">
                        <Link title="Create new employee" to={"employee-editor/create"}><AddIcon fontSize='large' /></Link>
                    </div>
                    <div className="col-1">
                        <Button title="Refresh table" onClick={() => this.loadEmployees()}><RefreshIcon fontSize='large' /></Button>
                    </div>
                </div>
                <Table size="small" className={this.classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell>Post</TableCell>
                            <TableCell>Phone</TableCell>
                            <TableCell>Organisational Unit</TableCell>
                            <TableCell>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {list.map(employee =>
                            <TableRow hover key={employee.id}>
                                <TableCell>{employee.id}</TableCell>
                                <TableCell>{employee.name}</TableCell>
                                <TableCell>{employee.post}</TableCell>
                                <TableCell>{employee.phone}</TableCell>
                                <TableCell title={employee.organisationalUnitName}>{employee.organisationalUnitShortName}</TableCell>
                                <TableCell>
                                    <Link to={"employee-editor/read/" + employee.id}><VisibilityIcon/></Link>
                                    <Link to={"employee-editor/update/" + employee.id}><EditIcon/></Link>
                                    <button className="borderlessbtn" onClick={() => this.delete(employee.id)}><DeleteIcon/> </button>
                                </TableCell>
                            </TableRow >
                        )}
                    </TableBody>
                </Table>
                <div className="row">
                    <div className="col-11">
                        <TablePagination
                            component="div"
                            count={this.count()}
                            page={this.state.page  - 1 }
                            onChangePage={this.handlePageChanged}
                            rowsPerPage={this.state.size}
                            rowsPerPageOptions={[5, 10, 25, 50, 100]}
                            onChangeRowsPerPage={this.handleChangePageSize}
                        />
                        </div>
                </div>
            </TableContainer>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderEmployeeTable(this.state.employees);

        return (
            <div>
                <h1 id="tabelLabel">Employees</h1>
                {contents}
            </div>
        );
    }

    async loadEmployees() {
        const response = await employeeService.list(this.state.page, this.state.size);
        const data = await response.json();

        await this.setState({ employees: data.records, loading: false, total: data.totalPages });
    }

    async delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Employee will be deleted!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(async (result) => {
            if (result.isConfirmed) {
                const response = await employeeService.delete(id);

                const data = await response.json();
                if (data.hasError) {
                    Swal.fire({
                        icon: 'error',
                        text: data.errorMessage
                    });
                } else {
                    await this.loadEmployees();
                    Swal.fire(
                        'Deleted!',
                        'Employee has been deleted.',
                        'success'
                    )
                }
            }
        })
    }
}