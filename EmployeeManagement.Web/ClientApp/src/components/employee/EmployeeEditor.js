﻿import React, { Component } from 'react';
import Swal from 'sweetalert2'
import employeeService from './../api-bussiness/EmployeeService'
import organisationalUnitService from './../api-bussiness/OrganisationalUnitService'
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import FormLabel from '@material-ui/core/FormLabel';

export class EmployeeEditor extends Component {
    static displayName = EmployeeEditor.name;

    constructor(props) {
        super(props);
        this.crudType = this.props.match.params.crudType;
        this.state = { id: 0, name: "", post: "", phone: "", userName: "", password:"", confirmPassword:"", principalId: null, organisationalUnitId: 0, organisationalUnitNames:[], employeeNames: [] };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeSelectList = this.handleChangeSelectList.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.navigateBack = this.navigateBack.bind(this);
    }

    isReadOnly() {
        return this.crudType === 'read';
    }

    navigate(target) {
        this.props.history.push(target);
    }

    navigateBack() {
        this.navigate("/employees")
    }

    componentDidMount() {
        if (this.crudType === "read" || this.crudType === "update") {
            this.readEmployee(this.props.match.params.id);
        }

        this.getOrganisationalUnitNames();
        this.getEmployees();
    }

    async handleSubmit(event) {
        event.preventDefault();

        let response;

        switch (this.crudType) {
            case 'create': response = await employeeService.create(this.state); break;
            case 'update': response = await employeeService.update(this.state); break;
            default : return;
        }
       
        const data = await response.json();
        if (data.hasError) {
            Swal.fire({
                icon: 'error',
                text: data.errorMessage
            });
        } else {
            Swal.fire({
                icon: 'success',
                text: 'SUCCESS'
            });

            this.navigateBack();
        }
    }

    handleChange(evt) {
        let value = evt.target.value;
        this.setState({
            ...this.state,
            [evt.target.name]: value
        });
    }

    handleChangeSelectList(evt, val) {
        let value = val.props.value;
        this.setState({
            ...this.state,
            [evt.target.name]: value
        });
    }

    render() {
        return (
            <Grid>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" onClick={() => this.navigate("/")}>
                        Home
                    </Link>
                    <Link color="inherit" onClick={() => this.navigate("/employees")}>
                        Employees
                     </Link>
                    <Typography color="textPrimary">{this.crudType.toUpperCase()} Employee</Typography>
                </Breadcrumbs>
                <Paper elevation={3} style={{ padding: "50px" }}>
                <form onSubmit={this.handleSubmit} autoComplete="off">
                        <Grid>
                            <Grid>
                                <TextField label="Id" type="number" value={this.id} hidden />
                            </Grid>
                            <Grid style={{ padding: "10px" }}>
                                <TextField
                                    required
                                    label="Name"
                                    name="name"
                                    type="text"
                                    value={this.state.name}
                                    onChange={this.handleChange}
                                    disabled={this.isReadOnly()}
                                />
                            </Grid>
                            <Grid style={{ padding: "10px" }}>
                                <TextField
                                    required
                                    label="Post"
                                    name="post"
                                    type="text"
                                    value={this.state.post}
                                    onChange={this.handleChange}
                                    disabled={this.isReadOnly()}
                                />
                            </Grid>
                            <Grid style={{ padding: "10px" }}>
                                <TextField
                                    required
                                    label="Phone"
                                    name="phone"
                                    type="text"
                                    value={this.state.phone}
                                    onChange={this.handleChange}
                                    disabled={this.isReadOnly()
                                    } />
                            </Grid>
                            {this.crudType === "create" &&
                                <Grid>
                                    <Grid style={{ padding: "10px" }}>
                                    <TextField
                                        required
                                        label="User name"
                                        name="userName"
                                        type="text"
                                        value={this.state.userName}
                                        autoComplete="new-password"
                                        onChange={this.handleChange}
                                    />
                                    </Grid>
                                    <Grid style={{ padding: "10px" }}>
                                    <TextField
                                        required
                                        label="Password"
                                        name="password"
                                        type="password"
                                        value={this.state.password}
                                        autoComplete="new-password"
                                        onChange={this.handleChange} />
                                    </Grid>
                                    <Grid style={{ padding: "10px" }}>
                                    <TextField
                                        required
                                        label="Confirm password"
                                        name="confirmPassword"
                                        type="password"
                                        value={this.state.confirmPassword}
                                        autoComplete="off"
                                        onChange={this.handleChange} />
                                    </Grid>
                                </Grid>
                            }
                            <Grid style={{ padding: "10px" }}>
                                <FormLabel component="legend">Principal</FormLabel>
                                <Select
                                    disabled={this.isReadOnly()}
                                    name="principalId"
                                    value={this.state.principalId === null || this.state.principalId === 0 ? '' : this.state.principalId}
                                    onChange={this.handleChangeSelectList}>
                                    {this.state.employeeNames.filter(x => x.id != this.state.id).map(employee =>
                                        <MenuItem key={employee.id} value={employee.id}>{employee.name}</MenuItem>)
                                    }
                                    <MenuItem value="null">None</MenuItem>
                                </Select>
                            </Grid>
                            <Grid style={{ padding: "10px" }}>
                                <FormLabel component="legend">Organisational unit*</FormLabel>
                                <Select
                                    required
                                    label="Organisational unit"
                                    disabled={this.isReadOnly()}
                                    name="organisationalUnitId"
                                    value={this.state.organisationalUnitId === 0 ? '' : this.state.organisationalUnitId}
                                    onChange={this.handleChangeSelectList}>
                                    {this.state.organisationalUnitNames.map(orgUnit =>
                                        <MenuItem key={orgUnit.id} value={orgUnit.id} title="">
                                            <span title={orgUnit.name}>{orgUnit.shortName}</span>
                                        </MenuItem>)
                                    }
                                </Select>
                            </Grid>
                            <Grid style={{ padding: "10px" }}>
                                {!this.isReadOnly() &&
                                    <Button color="primary" type="submit" >Submit</Button>
                                }
                                <Button onClick={() => this.navigateBack()}>Back</Button>
                            </Grid>
                        </Grid>
                    </form>
                </Paper>
            </Grid>
        );
    }

    async readEmployee(id) {
        const response = await employeeService.read(id);

        const data = await response.json();
        if (data.hasError) {
            Swal.fire({
                icon: 'error',
                text: data.errorMessage
            });
        } else {
            this.setState({ id: data.id, name: data.name, organisationalUnitId: data.organisationalUnitId, phone: data.phone, post: data.post, principalId: data.principalId });
        }
    }

    async getOrganisationalUnitNames() {
        const response = await organisationalUnitService.listOrganisationalUnitNames();

        const data = await response.json();
        if (data.hasError) {
            Swal.fire({
                icon: 'error',
                text: data.errorMessage
            });
        } else {
            this.setState({ organisationalUnitNames: data.records });
        }
    }

    async getEmployees() {
        const response = await employeeService.listEmployeeNames();

        const data = await response.json();
        if (data.hasError) {
            Swal.fire({
                icon: 'error',
                text: data.errorMessage
            });
        } else {
            this.setState({ employeeNames: data.records });
        }
    }
}