﻿import React, { Component } from 'react';
import Swal from 'sweetalert2'
import organisationalUnitService from './../api-bussiness/OrganisationalUnitService'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

export class OranisationalUnitEditor extends Component {
    static displayName = OranisationalUnitEditor.name;

    constructor(props) {
        super(props);
        this.crudType = this.props.match.params.crudType;

        this.state = { id: 0, name: "", shortName: "" };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.navigateBack = this.navigateBack.bind(this);
    }

    isReadOnly() {
        return this.crudType === 'read';
    }

    navigate(target) {
        this.props.history.push(target);
    }

    navigateBack() {
        this.navigate("/organisational_units")
    }

    componentDidMount() {
        if (this.crudType === "read" || this.crudType === "update") {
            this.readOrganisationalUnit(this.props.match.params.id);
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        let response;
        switch (this.crudType) {
            case 'create': response = await organisationalUnitService.create(this.state); break;
            case 'update': response = await organisationalUnitService.update(this.state); break;
            default: return;
        }

        const data = await response.json();
        if (data.hasError) {
            Swal.fire({
                icon: 'error',
                text: data.errorMessage
            });
        } else {
            Swal.fire({
                icon: 'success',
                text: 'SUCCESS'
            });

            this.navigateBack();
        }
    }

    handleChange(evt) {
        let value = evt.target.value;
        this.setState({
            ...this.state,
            [evt.target.name]: value
        });
    }

    render() {
        return (
            <Grid>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" onClick={() => this.navigate("/")}>
                        Home
                    </Link>
                    <Link color="inherit" onClick={() =>this.navigate("/organisational_units")}>
                        Organisational Units
                     </Link>
                    <Typography color="textPrimary">{this.crudType.toUpperCase()} Organisational unit</Typography>
                </Breadcrumbs>
                <Paper elevation={3} style={{ padding: "50px" }}>
                    <form onSubmit={this.handleSubmit}>
                        <Grid>
                            <Grid>
                                <TextField type="number" value={this.id} hidden />
                            </Grid>
                            <Grid style={{ padding: "10px" }}>
                                <TextField
                                    required
                                    label="Name"
                                    name="name"
                                    type="text"
                                    value={this.state.name}
                                    onChange={this.handleChange}
                                    disabled={this.isReadOnly()}
                                        />
                            </Grid>
                            <Grid style={{ padding: "10px" }}>
                                <TextField label="Short name"
                                    required
                                    name="shortName"
                                    type="text"
                                    value={this.state.shortName}
                                    onChange={this.handleChange}
                                    disabled={this.isReadOnly()}
                                    />
                            </Grid>
                            <Grid style={{ padding: "10px" }}>
                                 {!this.isReadOnly() &&
                                    <Button color="primary" type="submit" >Submit</Button>
                                 }
                                <Button onClick={() => this.navigateBack()}>Back</Button>
                            </Grid>
                        </Grid>
                    </form>
                </Paper>
            </Grid>
        );
    }

    async readOrganisationalUnit(id) {
        var response = await organisationalUnitService.read(id);

        const data = await response.json();
        if (data.hasError) {
            Swal.fire({
                icon: 'error',
                text: data.errorMessage
            });
        } else {
            this.setState({ id: data.id, name: data.name, shortName: data.shortName });
        }

    }
}
