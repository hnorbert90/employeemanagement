﻿import React, { Component } from 'react';
import Swal from 'sweetalert2';
import { Link } from "react-router-dom";
import organisationalUnitService from './../api-bussiness/OrganisationalUnitService'
import TablePagination from '@material-ui/core/TablePagination';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import RefreshIcon from '@material-ui/icons/Refresh';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

export class OrganisationalUnit extends Component {
    static displayName = OrganisationalUnit.name;

  constructor(props) {
    super(props);
      this.state = { organisationalUnits: [], loading: true, page: 1, total: 1, size: 10 };

      this.handlePageChanged = this.handlePageChanged.bind(this);
      this.handleChangePageSize = this.handleChangePageSize.bind(this);
      this.count = this.count.bind(this);

      this.classes = makeStyles({
          table: {
              maxWidth: 650,
          }
      });
    }

    componentDidMount() {
        this.loadOrganisationalUnits();
    }

    async handlePageChanged(e, value) {
        await this.setState({ page: value + 1 });
        await this.loadOrganisationalUnits();
    }

    async handleChangePageSize(e, value) {
        await this.setState({ size: value.props.value });
        await this.loadOrganisationalUnits();
    }

    count() {
        return this.state.size * this.state.total;
    }

    renderOrganisationalUnits(list) {
        return (
            <TableContainer component={Paper}>
                <div className="row justify-content-between pt-2">
                    <div className="col-1">
                        <Link title="Create new organisational unit" to={"oranisational-unit-editor/create"}><AddIcon fontSize='large' /></Link>
                    </div>
                    <div className="col-1">
                        <Button title="Refresh table" onClick={() => this.loadOrganisationalUnits()}><RefreshIcon fontSize='large' /></Button>
                    </div>
                </div>
                <Table size="small" className={this.classes.table} aria-labelledby="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell>ShortName</TableCell>
                            <TableCell>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {list.map(orgUnit =>
                        <TableRow hover key={orgUnit.id}>
                            <TableCell>{orgUnit.id}</TableCell>
                            <TableCell>{orgUnit.name}</TableCell>
                            <TableCell>{orgUnit.shortName}</TableCell>
                            <TableCell>
                                <Link to={"oranisational-unit-editor/read/" + orgUnit.id}><VisibilityIcon /></Link>
                                <Link to={"oranisational-unit-editor/update/" + orgUnit.id}><EditIcon /></Link>
                                <button className="borderlessbtn" onClick={() => this.delete(orgUnit.id)}><DeleteIcon/></button>
                            </TableCell>
                        </TableRow>
                    )}
                    </TableBody>
                </Table>
            <div className="row">
                <div className="col-11">
                    <TablePagination
                        component="div"
                        count={this.count()}
                        page={this.state.page - 1}
                        onChangePage={this.handlePageChanged}
                        rowsPerPage={this.state.size}
                        rowsPerPageOptions={[5, 10, 25, 50, 100]}
                        onChangeRowsPerPage={this.handleChangePageSize}
                    />
                </div>
            </div>
        </TableContainer>
    );
  }

  render() {
    let contents = this.state.loading
        ? <p><em>Loading...</em></p>
        : this.renderOrganisationalUnits(this.state.organisationalUnits);

    return (
      <div>
        <h1 id="tabelLabel">Organisational Units</h1>
        {contents}
      </div>
    );
  }

  async loadOrganisationalUnits() {
      const response = await organisationalUnitService.list(this.state.page, this.state.size);
      var data = await response.json();
      if (data.hasError) {
          Swal.fire({
              icon: 'error',
              text: data.errorMessage
          });
      } else {
          await this.setState({ organisationalUnits: data.records, loading: false, total: data.totalPages });
      }
    }

    async delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Organisational unit will be deleted!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(async (result) => {
            if (result.isConfirmed) {
                const response = await organisationalUnitService.delete(id);
                const data = await response.json();
                if (data.hasError) {
                    Swal.fire({
                        icon: 'error',
                        text: data.errorMessage
                    });
                } else {
                    await this.loadOrganisationalUnits();
                    Swal.fire(
                        'Deleted!',
                        'Organisational unit has been deleted.',
                        'success'
                    )
                }
            }
        })
    }
}