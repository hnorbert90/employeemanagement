﻿using System.Security.Claims;

using EmployeeManagement.Business.Common;
using EmployeeManagement.Persistence.Entities;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace EmployeeManagement.Web
{
    public class SessionDataProvider : ISessionDataProvider
    {
        private readonly IHttpContextAccessor _httpContextAcessor;
        private readonly UserManager<ApplicationUser> _userManager;

        public SessionDataProvider(IHttpContextAccessor httpContextAcessor, UserManager<ApplicationUser> userManager)
        {
            _httpContextAcessor = httpContextAcessor;
            _userManager = userManager;
        }

        public string Username
        {
            get
            {
                var idString = _httpContextAcessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                var user = _userManager.FindByIdAsync(idString).Result;
                return user.UserName;
            }
        }

        public long LoggedUserID
        {
            get
            {
                var idString = _httpContextAcessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                var user = _userManager.FindByIdAsync(idString).Result;
                return user?.EmployeeID ?? default;
            }
        }
    }
}
