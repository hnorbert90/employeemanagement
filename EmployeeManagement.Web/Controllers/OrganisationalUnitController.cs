﻿namespace EmployeeManagement.Web.Controllers
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Commands;
    using EmployeeManagement.Business.Common;
    using EmployeeManagement.Business.Queries;

    using HS.Mediator.Common;

    using MediatR;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class OrganisationalUnitController : ControllerBase
    {
        public OrganisationalUnitController(IMediator mediator, ISessionDataProvider sessionDataProvider) : base(mediator, sessionDataProvider)
        {
        }

        [HttpGet]
        public async Task<ReadOrganisationalUnitQueryResponse> Get(long id, CancellationToken cancellationToken = default)
        {
            return await Mediator.Send(new ReadOrganisationalUnitQuery { Id = id, LoggedUserID = SessionDataProvider.LoggedUserID }, cancellationToken);
        }

        [HttpGet("List")]
        public async Task<ListOrganisationalUnitsQueryResponse> List(int pageIndex = 1, int pageSize = 10, CancellationToken cancellationToken = default)
        {
            return await Mediator.Send(new ListOrganisationalUnitsQuery { PageIndex = pageIndex, PageSize = pageSize, LoggedUserID = SessionDataProvider.LoggedUserID }, cancellationToken);
        }

        [HttpGet("ListOrganisationalUnitNames")]
        public async Task<ListOrganisationalUnitNamesQueryResponse> ListOrganisationalUnitNames(CancellationToken cancellationToken = default)
        {
            var resp = await Mediator.Send(new ListOrganisationalUnitNamesQuery { LoggedUserID = SessionDataProvider.LoggedUserID }, cancellationToken);
            return resp;
        }

        [HttpPost("Create")]
        public async Task<ResponseBase<long>> Create(CreateOrganisationalUnitCommand command, CancellationToken cancellationToken = default)
        {
            command.LoggedUserID = SessionDataProvider.LoggedUserID;

            return await Mediator.Send(command, cancellationToken);
        }

        [HttpPut("Update")]
        public async Task<ResponseBase> Update(UpdateOrganisationalUnitCommand command, CancellationToken cancellationToken = default)
        {
            command.LoggedUserID = SessionDataProvider.LoggedUserID;

            return await Mediator.Send(command, cancellationToken);
        }

        [HttpDelete("Delete")]
        public async Task<ResponseBase> Delete(long id, CancellationToken cancellationToken = default)
        {
            return await Mediator.Send(new DeleteOrganisationalUnitCommand { Id = id, LoggedUserID = SessionDataProvider.LoggedUserID }, cancellationToken);
        }
    }
}
