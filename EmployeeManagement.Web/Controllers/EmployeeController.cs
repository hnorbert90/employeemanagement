﻿namespace EmployeeManagement.Web.Controllers
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Commands;
    using EmployeeManagement.Business.Common;
    using EmployeeManagement.Business.Queries;

    using HS.Mediator.Common;

    using MediatR;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        public EmployeeController(IMediator mediator, ISessionDataProvider sessionDataProvider) : base(mediator, sessionDataProvider)
        {
        }

        [HttpGet]
        public async Task<ReadEmployeeQueryResponse> Get(long id, CancellationToken cancellationToken = default)
        {
            return await Mediator.Send(new ReadEmployeeQuery { Id = id, LoggedUserID = SessionDataProvider.LoggedUserID }, cancellationToken);
        }

        [HttpGet("List")]
        public async Task<ListEmployeesQueryResponse> List(int pageIndex = 1, int pageSize = 10, CancellationToken cancellationToken = default)
        {
            return await Mediator.Send(new ListEmployeesQuery { PageIndex = pageIndex, PageSize = pageSize, LoggedUserID = SessionDataProvider.LoggedUserID }, cancellationToken);
        }

        [HttpGet("ListEmployeeNames")]
        public async Task<ListEmployeeNamesQueryResponse> ListEmployeeNames(CancellationToken cancellationToken = default)
        {
            return await Mediator.Send(new ListEmployeeNamesQuery { LoggedUserID = SessionDataProvider.LoggedUserID }, cancellationToken);
        }

        [HttpPost("Create")]
        public async Task<ResponseBase<long>> Create(CreateEmployeeCommand command, CancellationToken cancellationToken = default)
        {
            command.LoggedUserID = SessionDataProvider.LoggedUserID;

            return await Mediator.Send(command, cancellationToken);
        }

        [HttpPut("Update")]
        public async Task<ResponseBase> Update([FromBody] UpdateEmployeeCommand command, CancellationToken cancellationToken = default)
        {
            command.LoggedUserID = SessionDataProvider.LoggedUserID;

            return await Mediator.Send(command, cancellationToken);
        }

        [HttpDelete("Delete")]
        public async Task<ResponseBase> Delete(long id, CancellationToken cancellationToken = default)
        {
            return await Mediator.Send(new DeleteEmployeeCommand { Id = id, LoggedUserID = SessionDataProvider.LoggedUserID }, cancellationToken);
        }
    }
}