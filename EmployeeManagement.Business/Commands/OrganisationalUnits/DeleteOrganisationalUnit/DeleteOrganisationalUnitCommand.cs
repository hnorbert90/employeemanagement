﻿namespace EmployeeManagement.Business.Commands
{

    using HS.Mediator.Common;

    public class DeleteOrganisationalUnitCommand : RequestBase<ResponseBase>
    {
        public long Id { get; set; }
    }
}
