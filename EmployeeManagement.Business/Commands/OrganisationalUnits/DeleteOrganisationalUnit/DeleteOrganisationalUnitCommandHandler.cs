﻿namespace EmployeeManagement.Business.Commands
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;
    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    using Microsoft.EntityFrameworkCore;

    public class DeleteOrganisationalUnitCommandHandler : HandlerBase<DeleteOrganisationalUnitCommand, ResponseBase>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteOrganisationalUnitCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ResponseBase> Handle(DeleteOrganisationalUnitCommand request, CancellationToken cancellationToken)
        {
            var query = _unitOfWork.GenericRepository.Query<OrganisationalUnit>(x => x.Id == request.Id).Include(x => x.Employees);
            var entity = await query.FirstOrDefaultAsync(cancellationToken);
            if (entity is null)
            {
                return ResponseBase.Error(ServiceError.ENTITY_NOT_FOUND);
            }

            if (entity.IsDeleted)
            {
                return ResponseBase.Error(ServiceError.ENTITY_ALREADY_DELETED);
            }

            if (entity.Employees.Any())
            {
                return ResponseBase.Error(ServiceError.ENTITY_REFERENCED_CANNOT_DELETE);
            }

            _unitOfWork.GenericRepository.Delete(entity);
            entity.DeletedBy = request.LoggedUserID.GetValueOrDefault();

            await _unitOfWork.SaveAsync(cancellationToken);

            return ResponseBase.Ok();
        }
    }
}