﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    public class CreateOrganisationalUnitCommandHandler : HandlerBase<CreateOrganisationalUnitCommand, ResponseBase<long>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrganisationalUnitCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ResponseBase<long>> Handle(CreateOrganisationalUnitCommand request, CancellationToken cancellationToken)
        {
            var entity = new OrganisationalUnit
            {
                CreatedBy = request.LoggedUserID.GetValueOrDefault(),
                Name = request.Name,
                ShortName = request.ShortName
            };

            await _unitOfWork.GenericRepository.CreateAsync(entity, cancellationToken);
            await _unitOfWork.SaveAsync(cancellationToken);

            return ResponseBase<long>.Ok(entity.Id);
        }
    }
}
