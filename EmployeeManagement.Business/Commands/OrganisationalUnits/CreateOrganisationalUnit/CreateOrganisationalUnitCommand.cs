﻿namespace EmployeeManagement.Business.Commands
{
    using HS.Mediator.Common;

    public class CreateOrganisationalUnitCommand : RequestBase<ResponseBase<long>>
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
