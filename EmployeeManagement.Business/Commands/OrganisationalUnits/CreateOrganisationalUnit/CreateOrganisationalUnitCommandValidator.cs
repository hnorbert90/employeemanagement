﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;

    using HS.Mediator.Common;

    public class CreateOrganisationalUnitCommandValidator : RequestValidatorBase<CreateOrganisationalUnitCommand, ResponseBase<long>>
    {
        public override Task<ResponseBase<long>> ValidateAsync(CreateOrganisationalUnitCommand request, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(request.Name))
            {
                return new ResponseBase<long> { ErrorMessage = ValidationError.NAME_REQUIRED };
            }

            if (string.IsNullOrWhiteSpace(request.ShortName))
            {
                return new ResponseBase<long> { ErrorMessage = ValidationError.SHORTNAME_REQUIRED };
            }

            return new ResponseBase<long>();
        }
    }
}
