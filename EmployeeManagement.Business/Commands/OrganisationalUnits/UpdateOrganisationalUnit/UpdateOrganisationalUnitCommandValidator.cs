﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;

    using HS.Mediator.Common;

    public class UpdateOrganisationalUnitCommandValidator : RequestValidatorBase<UpdateOrganisationalUnitCommand, ResponseBase>
    {
        public override Task<ResponseBase> ValidateAsync(UpdateOrganisationalUnitCommand request, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(request.Name))
            {
                return ResponseBase.Error(ValidationError.NAME_REQUIRED);
            }

            if (string.IsNullOrWhiteSpace(request.ShortName))
            {
                return ResponseBase.Error(ValidationError.SHORTNAME_REQUIRED);
            }

            return ResponseBase.Ok();
        }
    }
}
