﻿namespace EmployeeManagement.Business.Commands
{

    using HS.Mediator.Common;

    public class UpdateOrganisationalUnitCommand : RequestBase<ResponseBase>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
