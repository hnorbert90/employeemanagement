﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;
    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    public class UpdateOrganisationalUnitCommandHandler : HandlerBase<UpdateOrganisationalUnitCommand, ResponseBase>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateOrganisationalUnitCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ResponseBase> Handle(UpdateOrganisationalUnitCommand request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.GenericRepository.ReadAsync<OrganisationalUnit>(x => x.Id == request.Id);
            if (entity is null)
            {
                return ResponseBase.Error(ServiceError.ENTITY_NOT_FOUND);
            }

            if (entity.IsDeleted)
            {
                return ResponseBase.Error(ServiceError.ENTITY_DELETED);
            }

            entity.ModifiedBy = request.LoggedUserID.GetValueOrDefault();
            entity.Name = request.Name;
            entity.ShortName = request.ShortName;

            await _unitOfWork.SaveAsync(cancellationToken);

            return ResponseBase.Ok();
        }
    }
}