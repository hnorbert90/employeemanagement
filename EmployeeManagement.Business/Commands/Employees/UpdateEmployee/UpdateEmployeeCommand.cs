﻿namespace EmployeeManagement.Business.Commands
{

    using HS.Mediator.Common;

    public class UpdateEmployeeCommand : RequestBase<ResponseBase>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Post { get; set; }
        public string Phone { get; set; }
        public long? PrincipalId { get; set; }
        public long OrganisationalUnitId { get; set; }
    }
}
