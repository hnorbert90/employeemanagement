﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;
    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    public class UpdateEmployeeCommandHandler : HandlerBase<UpdateEmployeeCommand, ResponseBase>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateEmployeeCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ResponseBase> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.GenericRepository.ReadAsync<Employee>(x => x.Id == request.Id);
            if (entity is null)
            {
                return ResponseBase.Error(ServiceError.ENTITY_NOT_FOUND);
            }

            if (entity.IsDeleted)
            {
                return ResponseBase.Error(ServiceError.ENTITY_DELETED);
            }

            entity.ModifiedBy = request.LoggedUserID.GetValueOrDefault();
            entity.Name = request.Name;
            entity.OrganisationalUnitId = request.OrganisationalUnitId;
            entity.Phone = (PhoneNumber)request.Phone;
            entity.Post = request.Post;
            entity.PrincipalId = request.PrincipalId;

            await _unitOfWork.SaveAsync(cancellationToken);

            return ResponseBase.Ok();
        }
    }
}