﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;

    using HS.Mediator.Common;

    public class UpdateEmployeeCommandValidator : RequestValidatorBase<UpdateEmployeeCommand, ResponseBase>
    {
        public override Task<ResponseBase> ValidateAsync(UpdateEmployeeCommand request, CancellationToken cancellationToken = default)
        {
            if (request.OrganisationalUnitId == 0)
            {
                return ResponseBase.Error(ValidationError.ORGANISATIONAL_UNIT_ID_REQUIRED);
            }

            if (string.IsNullOrWhiteSpace(request.Name))
            {
                return ResponseBase.Error(ValidationError.NAME_REQUIRED);
            }

            if (request.PrincipalId == 0)
            {
                return ResponseBase.Error(ValidationError.PRINCIPAL_ID_CANNOT_BE_ZERO);
            }

            if (request.PrincipalId == request.Id)
            {
                return ResponseBase.Error(ValidationError.EMPLOYEE_CANNOT_BE_SELF_PRINCIPAL);
            }

            return ResponseBase.Ok();
        }
    }
}
