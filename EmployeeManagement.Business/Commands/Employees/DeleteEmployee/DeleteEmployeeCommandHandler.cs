﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;
    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    using MediatR;

    public class DeleteEmployeeCommandHandler : HandlerBase<DeleteEmployeeCommand, ResponseBase>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public DeleteEmployeeCommandHandler(IUnitOfWork unitOfWork, IMediator mediator)
        {
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }

        public async override Task<ResponseBase> Handle(DeleteEmployeeCommand request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.GenericRepository.ReadAsync<Employee>(x => x.Id == request.Id, cancellationToken);
            if (entity is null)
            {
                return ResponseBase.Error(ServiceError.ENTITY_NOT_FOUND);
            }

            if (entity.IsDeleted)
            {
                return ResponseBase.Error(ServiceError.ENTITY_ALREADY_DELETED);
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync(cancellationToken);

                _unitOfWork.GenericRepository.Delete(entity);
                entity.DeletedBy = request.LoggedUserID.GetValueOrDefault();

                var response = await _mediator.Send(
                    new LockoutUserCommand 
                    { 
                        EmployeeId = request.Id,
                        LoggedUserID = request.LoggedUserID
                    },
                    cancellationToken);

                if (response.HasError)
                {
                    await _unitOfWork.RollbackAsync(cancellationToken);

                    return response;
                }

                await _unitOfWork.SaveAsync(cancellationToken);
                await _unitOfWork.CommitAsync(cancellationToken);

                return ResponseBase.Ok();
            }
            catch
            {
                await _unitOfWork.RollbackAsync(cancellationToken);

                throw;
            }
        }
    }
}