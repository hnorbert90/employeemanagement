﻿namespace EmployeeManagement.Business.Commands
{

    using HS.Mediator.Common;

    public class DeleteEmployeeCommand : RequestBase<ResponseBase>
    {
        public long Id { get; set; }
    }
}