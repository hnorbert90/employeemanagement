﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;

    using HS.Mediator.Common;

    public class CreateEmployeeCommandValidator : RequestValidatorBase<CreateEmployeeCommand, ResponseBase<long>>
    {
        public override Task<ResponseBase<long>> ValidateAsync(CreateEmployeeCommand request, CancellationToken cancellationToken = default)
        {
            if (request.OrganisationalUnitId == 0)
            {
                return new ResponseBase<long> { ErrorMessage = ValidationError.ORGANISATIONAL_UNIT_ID_REQUIRED };
            }

            if (string.IsNullOrWhiteSpace(request.Name))
            {
                return new ResponseBase<long> { ErrorMessage = ValidationError.NAME_REQUIRED };
            }

            return new ResponseBase<long>();
        }
    }
}
