﻿namespace EmployeeManagement.Business.Commands
{

    using HS.Mediator.Common;

    public class CreateEmployeeCommand : RequestBase<ResponseBase<long>>
    {
        public string Name { get; set; }
        public string Post { get; set; }
        public string Phone { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long? PrincipalId { get; set; }
        public long OrganisationalUnitId { get; set; }
    }
}
