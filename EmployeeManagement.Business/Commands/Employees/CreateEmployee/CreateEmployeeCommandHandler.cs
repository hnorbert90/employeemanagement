﻿namespace EmployeeManagement.Business.Commands
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    using MediatR;

    public class CreateEmployeeCommandHandler : HandlerBase<CreateEmployeeCommand, ResponseBase<long>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public CreateEmployeeCommandHandler(IUnitOfWork unitOfWork, IMediator mediator)
        {
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }

        public async override Task<ResponseBase<long>> Handle(CreateEmployeeCommand request, CancellationToken cancellationToken)
        {
            var entity = new Employee
            {
                CreatedBy = request.LoggedUserID.GetValueOrDefault(),
                Name = request.Name,
                OrganisationalUnitId = request.OrganisationalUnitId,
                Phone = (PhoneNumber)request.Phone,
                Post = request.Post,
                PrincipalId = request.PrincipalId
            };

            try
            {
                await _unitOfWork.BeginTransactionAsync(cancellationToken);

                await _unitOfWork.GenericRepository.CreateAsync(entity);
                await _unitOfWork.SaveAsync(cancellationToken);

                if (entity.Id != 0)
                {
                    var response = await _mediator.Send(
                        new RegisterUserCommand
                        {
                            ConfirmPassword = request.ConfirmPassword,
                            EmployeeId = entity.Id,
                            LoggedUserID = request.LoggedUserID,
                            Password = request.Password,
                            UserName = request.UserName
                        });

                    if (response.HasError)
                    {
                        await _unitOfWork.RollbackAsync(cancellationToken);

                        return new ResponseBase<long> { ErrorMessage = response.ErrorMessage };
                    }
                }
                else
                {
                    throw new System.Exception("User was not created!");
                }

                await _unitOfWork.CommitAsync(cancellationToken);

                return ResponseBase<long>.Ok(entity.Id);

            }
            catch
            {
                await _unitOfWork.RollbackAsync(cancellationToken);

                throw;
            }
        }
    }
}
