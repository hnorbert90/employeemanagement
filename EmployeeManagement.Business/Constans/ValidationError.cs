﻿namespace EmployeeManagement.Business.Constans
{
    public static class ValidationError
    {
        public const string ORGANISATIONAL_UNIT_ID_REQUIRED = nameof(ORGANISATIONAL_UNIT_ID_REQUIRED);
        public const string PASSWORD_REQUIRED = nameof(PASSWORD_REQUIRED);
        public const string PASSWORD_MISSMATCH = nameof(PASSWORD_MISSMATCH);
        public const string INCORRECT_PASSWORD = nameof(INCORRECT_PASSWORD);
        public const string NAME_REQUIRED = nameof(NAME_REQUIRED);
        public const string USERNAME_REQUIRED = nameof(USERNAME_REQUIRED);
        public const string EMPLOYEE_CODE_REQUIRED = nameof(EMPLOYEE_CODE_REQUIRED);
        public const string EMPLOYEE_CODE_ALREADY_TAKEN = nameof(EMPLOYEE_CODE_ALREADY_TAKEN);
        public const string SHORTNAME_REQUIRED = nameof(SHORTNAME_REQUIRED);
        public const string EMPLOYEE_CANNOT_BE_SELF_PRINCIPAL = nameof(EMPLOYEE_CANNOT_BE_SELF_PRINCIPAL);
        public const string PRINCIPAL_ID_CANNOT_BE_ZERO = nameof(PRINCIPAL_ID_CANNOT_BE_ZERO);
    }
}
