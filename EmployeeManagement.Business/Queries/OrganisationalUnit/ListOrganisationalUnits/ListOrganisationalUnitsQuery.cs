﻿namespace EmployeeManagement.Business.Queries
{

    using HS.Mediator.Common;

    public class ListOrganisationalUnitsQuery : PagedQueryRequestBase<ListOrganisationalUnitsQueryResponse>
    {
    }
}
