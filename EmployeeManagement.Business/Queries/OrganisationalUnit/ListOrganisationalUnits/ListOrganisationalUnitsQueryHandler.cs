﻿namespace EmployeeManagement.Business.Queries
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Domain;

    using HS.Extensions.IQueryable;
    using HS.Mediator.Common;

    using Microsoft.EntityFrameworkCore;

    public class ListOrganisationalUnitsQueryHandler : HandlerBase<ListOrganisationalUnitsQuery, ListOrganisationalUnitsQueryResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ListOrganisationalUnitsQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ListOrganisationalUnitsQueryResponse> Handle(ListOrganisationalUnitsQuery request, CancellationToken cancellationToken)
        {
            var query = _unitOfWork.ReadOnlyRepository.Query<OrganisationalUnit>(o => !o.IsDeleted);

            return new ListOrganisationalUnitsQueryResponse
            {
                Records = await query.AsNoTracking().ToPaginatedResult(request).Select(o =>
                new ListOrganisationalUnitsQueryResponse.OrganisationalUnitDTO
                {
                    Id = o.Id,
                    Name = o.Name,
                    ShortName = o.ShortName
                }).ToListAsync(cancellationToken),
                TotalPages = query.GetTotalPages(request)
            };
        }
    }
}