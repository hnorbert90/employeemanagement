﻿namespace EmployeeManagement.Business.Queries
{

    using HS.Mediator.Common;

    public class ListOrganisationalUnitsQueryResponse : PagedQueryResponseBase<ListOrganisationalUnitsQueryResponse.OrganisationalUnitDTO>
    {
        public class OrganisationalUnitDTO
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
        }
    }
}
