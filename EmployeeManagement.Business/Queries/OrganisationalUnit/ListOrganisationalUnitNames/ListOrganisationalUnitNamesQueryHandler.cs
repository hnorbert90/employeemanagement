﻿namespace EmployeeManagement.Business.Queries
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    using Microsoft.EntityFrameworkCore;

    public class ListOrganisationalUnitNamesQueryHandler : HandlerBase<ListOrganisationalUnitNamesQuery, ListOrganisationalUnitNamesQueryResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ListOrganisationalUnitNamesQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ListOrganisationalUnitNamesQueryResponse> Handle(ListOrganisationalUnitNamesQuery request, CancellationToken cancellationToken)
        {
            var query = _unitOfWork.ReadOnlyRepository.Query<OrganisationalUnit>(x => !x.IsDeleted);

            return new ListOrganisationalUnitNamesQueryResponse
            {
                Records = await query.AsNoTracking().Select(e =>
                new ListOrganisationalUnitNamesQueryResponse.OrganisationalUnitNameDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    ShortName = e.ShortName
                }).ToListAsync(cancellationToken)
            };
        }
    }
}
