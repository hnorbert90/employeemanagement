﻿namespace EmployeeManagement.Business.Queries
{
    using HS.Mediator.Common;

    public class ListOrganisationalUnitNamesQueryResponse : ListQueryResponseBase<ListOrganisationalUnitNamesQueryResponse.OrganisationalUnitNameDTO>
    {
        public class OrganisationalUnitNameDTO
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
        }
    }
}
