﻿namespace EmployeeManagement.Business.Queries
{
    using HS.Mediator.Common;

    public class ListOrganisationalUnitNamesQuery : RequestBase<ListOrganisationalUnitNamesQueryResponse>
    {
    }
}
