﻿namespace EmployeeManagement.Business.Queries
{

    using HS.Mediator.Common;

    public class ReadOrganisationalUnitQueryResponse : ResponseBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
