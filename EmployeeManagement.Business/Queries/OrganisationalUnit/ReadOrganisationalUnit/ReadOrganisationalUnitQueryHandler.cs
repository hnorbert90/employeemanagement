﻿namespace EmployeeManagement.Business.Queries
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;
    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    public class ReadOrganisationalUnitQueryHandler : HandlerBase<ReadOrganisationalUnitQuery, ReadOrganisationalUnitQueryResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReadOrganisationalUnitQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ReadOrganisationalUnitQueryResponse> Handle(ReadOrganisationalUnitQuery request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.ReadOnlyRepository.ReadAsync<OrganisationalUnit>(x => x.Id == request.Id, cancellationToken);
            if (entity == null)
            {
                return new ReadOrganisationalUnitQueryResponse { ErrorMessage = ServiceError.ENTITY_NOT_FOUND };
            }

            if(entity.IsDeleted)
            {
                return new ReadOrganisationalUnitQueryResponse { ErrorMessage = ServiceError.ENTITY_DELETED };
            }

            return new ReadOrganisationalUnitQueryResponse
            {
                Id = entity.Id,
                Name = entity.Name,
                ShortName = entity.ShortName
            };
        }
    }
}