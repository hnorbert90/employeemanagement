﻿namespace EmployeeManagement.Business.Queries
{
    using HS.Mediator.Common;

    public class ReadOrganisationalUnitQuery : RequestBase<ReadOrganisationalUnitQueryResponse>
    {
        public long Id { get; set; }
    }
}
