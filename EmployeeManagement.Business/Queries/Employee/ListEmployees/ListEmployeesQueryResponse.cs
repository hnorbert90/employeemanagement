﻿namespace EmployeeManagement.Business.Queries
{
    using HS.Mediator.Common;

    public class ListEmployeesQueryResponse : PagedQueryResponseBase<ListEmployeesQueryResponse.EmployeeDTO>
    {
        public class EmployeeDTO
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public string Post { get; set; }
            public string Phone { get; set; }
            public string OrganisationalUnitShortName { get; set; }
            public string OrganisationalUnitName { get; set; }
        }
    }
}
