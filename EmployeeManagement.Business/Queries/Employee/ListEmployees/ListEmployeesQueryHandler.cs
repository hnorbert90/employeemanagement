﻿namespace EmployeeManagement.Business.Queries
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Domain;

    using HS.Extensions.IQueryable;
    using HS.Mediator.Common;

    using Microsoft.EntityFrameworkCore;

    public class ListEmployeesQueryHandler : HandlerBase<ListEmployeesQuery, ListEmployeesQueryResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ListEmployeesQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ListEmployeesQueryResponse> Handle(ListEmployeesQuery request, CancellationToken cancellationToken)
        {
            var query = _unitOfWork.ReadOnlyRepository.Query<Employee>(x => !x.IsDeleted).Include(x => x.OrganisationalUnit).AsNoTracking();

            return new ListEmployeesQueryResponse
            {
                Records = await query.ToPaginatedResult(request).Select(e =>
                new ListEmployeesQueryResponse.EmployeeDTO
                {
                    Id = e.Id,
                    Name = e.Name,
                    Post = e.Post,
                    Phone = e.Phone,
                    OrganisationalUnitShortName = e.OrganisationalUnit.ShortName,
                    OrganisationalUnitName = e.OrganisationalUnit.Name
                }).ToListAsync(cancellationToken),
                TotalPages = query.GetTotalPages(request)
            };
        }
    }
}
