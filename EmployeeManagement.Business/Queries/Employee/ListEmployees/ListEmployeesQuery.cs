﻿namespace EmployeeManagement.Business.Queries
{
    using HS.Mediator.Common;

    public class ListEmployeesQuery : PagedQueryRequestBase<ListEmployeesQueryResponse>
    {
    }
}
