﻿namespace EmployeeManagement.Business.Queries
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    using Microsoft.EntityFrameworkCore;

    public class ListEmployeeNamesQueryHandler : HandlerBase<ListEmployeeNamesQuery, ListEmployeeNamesQueryResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ListEmployeeNamesQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ListEmployeeNamesQueryResponse> Handle(ListEmployeeNamesQuery request, CancellationToken cancellationToken)
        {
            var query = _unitOfWork.ReadOnlyRepository.Query<Employee>(x => !x.IsDeleted);

            return new ListEmployeeNamesQueryResponse
            {
                Records = await query.AsNoTracking().Select(e =>
                new ListEmployeeNamesQueryResponse.EmployeeNameDTO
                {
                    Id = e.Id,
                    Name = e.Name
                }).ToListAsync(cancellationToken)
            };
        }
    }
}
