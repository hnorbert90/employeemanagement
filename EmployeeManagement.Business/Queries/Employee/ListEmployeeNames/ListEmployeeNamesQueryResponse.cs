﻿namespace EmployeeManagement.Business.Queries
{
    using HS.Mediator.Common;

    public class ListEmployeeNamesQueryResponse : ListQueryResponseBase<ListEmployeeNamesQueryResponse.EmployeeNameDTO>
    {
        public class EmployeeNameDTO
        {
            public long Id { get; set; }
            public string Name { get; set; }
        }
    }
}
