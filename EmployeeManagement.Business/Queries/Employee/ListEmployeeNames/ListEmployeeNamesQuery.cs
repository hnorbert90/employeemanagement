﻿namespace EmployeeManagement.Business.Queries
{
    using HS.Mediator.Common;

    public class ListEmployeeNamesQuery : PagedQueryRequestBase<ListEmployeeNamesQueryResponse>
    {
    }
}
