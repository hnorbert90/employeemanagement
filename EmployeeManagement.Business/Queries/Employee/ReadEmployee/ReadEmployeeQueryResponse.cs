﻿namespace EmployeeManagement.Business.Queries
{

    using HS.Mediator.Common;

    public class ReadEmployeeQueryResponse : ResponseBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long OrganisationalUnitId { get; set; }
        public string Phone { get; set; }
        public string Post { get; set; }
        public long? PrincipalId { get; set; }
    }
}
