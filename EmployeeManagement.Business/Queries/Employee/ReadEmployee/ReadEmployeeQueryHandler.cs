﻿namespace EmployeeManagement.Business.Queries
{
    using System.Threading;
    using System.Threading.Tasks;

    using EmployeeManagement.Business.Constans;
    using EmployeeManagement.Domain;

    using HS.Mediator.Common;

    public class ReadEmployeeQueryHandler : HandlerBase<ReadEmployeeQuery, ReadEmployeeQueryResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReadEmployeeQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async override Task<ReadEmployeeQueryResponse> Handle(ReadEmployeeQuery request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.ReadOnlyRepository.ReadAsync<Employee>(x => x.Id == request.Id, cancellationToken);
            if (entity == null)
            {
                return new ReadEmployeeQueryResponse { ErrorMessage = ServiceError.ENTITY_NOT_FOUND };
            }

            if(entity.IsDeleted)
            {
                return new ReadEmployeeQueryResponse { ErrorMessage = ServiceError.ENTITY_DELETED };
            }

            return new ReadEmployeeQueryResponse
            {
                Id = entity.Id,
                Name = entity.Name,
                OrganisationalUnitId = entity.OrganisationalUnitId,
                Phone = entity.Phone,
                Post = entity.Post,
                PrincipalId = entity.PrincipalId
            };
        }
    }
}