﻿namespace EmployeeManagement.Business.Queries
{
    using HS.Mediator.Common;

    public class ReadEmployeeQuery : RequestBase<ReadEmployeeQueryResponse>
    {
        public long Id { get; set; }
    }
}
