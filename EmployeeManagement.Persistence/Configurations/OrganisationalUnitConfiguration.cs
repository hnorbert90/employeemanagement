﻿namespace EmployeeManagement.Persistence.Configurations
{
    using EmployeeManagement.Domain;
    using EmployeeManagement.Persistence.Configurations.Base;

    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class OrganisationalUnitConfiguration : ConfigurationBase<OrganisationalUnit>
    {
        public override void Configure(EntityTypeBuilder<OrganisationalUnit> builder)
        {
            base.Configure(builder);

            builder.Property(o => o.ShortName).HasMaxLength(10);

            builder.HasMany(o => o.Employees)
                .WithOne(e => e.OrganisationalUnit)
                .HasForeignKey(e => e.OrganisationalUnitId);
        }
    }
}
