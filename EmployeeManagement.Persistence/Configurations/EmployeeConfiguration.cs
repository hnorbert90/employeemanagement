﻿namespace EmployeeManagement.Persistence.Configurations
{
    using EmployeeManagement.Domain;
    using EmployeeManagement.Persistence.Configurations.Base;

    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class EmployeeConfiguration : ConfigurationBase<Employee>
    {
        public override void Configure(EntityTypeBuilder<Employee> builder)
        {
            base.Configure(builder);
            builder.Property(e => e.OrganisationalUnitId).IsRequired();
            builder.OwnsOne(e => e.Phone);
            builder.Property(e => e.Post).IsRequired();
            builder.Property(e => e.PrincipalId);

            builder.HasOne(e => e.OrganisationalUnit)
                .WithMany(o => o.Employees)
                .HasForeignKey(e => e.OrganisationalUnitId);
        }
    }
}
