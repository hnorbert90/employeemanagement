﻿namespace EmployeeManagement.Persistence
{
    using EmployeeManagement.Domain;

    using Microsoft.EntityFrameworkCore;

    public class EmployeeManagementDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<OrganisationalUnit> OrganisationalUnits { get; set; }

        public EmployeeManagementDbContext(DbContextOptions<EmployeeManagementDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().OwnsOne(p => p.Phone);
            base.OnModelCreating(modelBuilder);
        }
    }
}
