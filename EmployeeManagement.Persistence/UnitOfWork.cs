﻿namespace EmployeeManagement.Persistence
{
    using EmployeeManagement.Domain;

    public class UnitOfWork : HS.Persistence.EntityFramework.UnitOfWork<EmployeeManagementDbContext>, IUnitOfWork
    {
        public UnitOfWork(EmployeeManagementDbContext dbContext) :base(dbContext)
        {
        }
    }
}