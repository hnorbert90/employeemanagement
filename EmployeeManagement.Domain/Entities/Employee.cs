﻿namespace EmployeeManagement.Domain
{
    public class Employee : Entity
    {
        public string Post { get; set; }
        public PhoneNumber Phone { get; set; }
        public long? PrincipalId { get; set; }
        public long OrganisationalUnitId { get; set; }

        public virtual OrganisationalUnit OrganisationalUnit { get; set; }
        public virtual Employee Principal { get; set; }
    }
}