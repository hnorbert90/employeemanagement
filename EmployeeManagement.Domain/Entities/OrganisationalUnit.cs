﻿using System.Collections.Generic;

namespace EmployeeManagement.Domain
{
    public class OrganisationalUnit : Entity
    {
        public string ShortName { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
